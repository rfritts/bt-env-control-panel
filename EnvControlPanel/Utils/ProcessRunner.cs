﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;

namespace EnvControlPanel.Utils
{
    public class ProcessRunner
    {
        public static string runRemoteCommand(string hostname, string username, string password, string command)
        {
            command = hostname.Equals("localhost") ?
                "psexec.exe \\\\" + hostname + " "  + command:
                "psexec.exe \\\\" + hostname + " -u " + username + " -p " + password + " " + command;
            var processInfo = new ProcessStartInfo("cmd.exe", "/c" + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;            
            var process = Process.Start(processInfo);
            string output = process.StandardOutput.ReadToEnd();
            string errorOut = process.StandardError.ReadToEnd();
            process.WaitForExit();
            process.Close();
            return StringUtils.stripPsexecText(output);
        }
    }
}
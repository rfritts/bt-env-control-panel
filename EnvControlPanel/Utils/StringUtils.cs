﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.Text;

namespace EnvControlPanel.Utils
{
    public class StringUtils
    {
        public static string keepOnlyIntegerCharacters(string input)
        {
            return Regex.Replace(input, "[^0-9]", "");
        }

        public static string encodeKeyValuePairAsJson(string key, string value)
        {
            var keyValues = new Dictionary<string, string>
            {
                {key, value}
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(keyValues);           
        }

        public static string stripPsexecText(string rawPsexecOutput)
        {
            var lines = Regex.Split(rawPsexecOutput, "\r\n|\r|\n");
            StringBuilder sb = new StringBuilder();
            foreach (string line in lines)
            {
                if ( (line.StartsWith("\r\nPsExec")) || (line.StartsWith("PsExec")) 
                    || (line.StartsWith("\r\nCopyright")) || (line.StartsWith("Copyright")) 
                    || (line.StartsWith("\r\nSysinternals")) || (line.StartsWith("Sysinternals")) ) 
                {
                    // do nothing
                } else
                {
                    sb.AppendLine(line);
                }
            }
            return sb.ToString();
        }
    }
}
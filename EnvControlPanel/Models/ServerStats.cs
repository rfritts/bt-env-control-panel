﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Text.RegularExpressions;
using EnvControlPanel.Utils;
using System.Management.Automation;

namespace EnvControlPanel.Models
{
    /// <summary>
    /// This class is responsible for curating the remote commands used to gather information about a remote server.
    /// </summary>
    public class ServerStats
    {

        static string username = "capstone";
        static string password = "Sch00lwork";

        public static string getServerCpuLoad(string hostname)
        {
            string command = "wmic cpu get loadpercentage";
            return StringUtils.keepOnlyIntegerCharacters(
                ProcessRunner.runRemoteCommand(hostname, username, password, command));
        }

        public static string getServerRamLoad(string hostname)
        {
            string command = "wmic OS get FreePhysicalMemory /Value";
            return StringUtils.keepOnlyIntegerCharacters(
                ProcessRunner.runRemoteCommand(hostname, username, password, command));
        }

        public static string getTotalProcessesRunning(string hostname)
        {
            string command = "tasklist | find /c /v \"\"";
            return ProcessRunner.runRemoteCommand(hostname, username, password, command);
        }


        public static int checkCriticalCpu(string cpuLoad)
        {
            int CPU = 0;
            if (!Int32.TryParse(cpuLoad, out CPU))
            {
                return 2; //conversion fail
            }

            if (CPU >= 35)
            {
                return 0; //heavy load
            }
            return 1; //no problem
        }

        public static int checkCriticalRam(string ramLoad)
        {
            int RAM = 0;
            if (!Int32.TryParse(ramLoad, out RAM))
            {
                return 2; //conversion fail
            }

            if (RAM < 1000000)
            {
                return 0; //heavy load
            }
            return 1; //no problem
        }

        public static void restartServer(string hostname)
        {
            string command = "shutdown.exe /r /f /t 00"; //Restart, force, time 0 seconds.  
                                                         //Time attribute can be changed to allow for cancellation requests.
            //ProcessRunner.runRemoteCommand(hostname, username, password, command);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EnvControlPanel.Utils;

namespace EnvControlPanel.Models
{
    public class RemoteCommandRunner
    {

        static string username = "capstone";
        static string password = "Sch00lwork";
        public string hostname { get; set; }
        public string command { get; set; }

        public string executeCustomRemoteCommand()
        {
            return ProcessRunner.runRemoteCommand(hostname, username, password, command);
        }
    }
}
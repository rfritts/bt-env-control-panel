﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EnvControlPanel.Startup))]
namespace EnvControlPanel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

        }
    }
}

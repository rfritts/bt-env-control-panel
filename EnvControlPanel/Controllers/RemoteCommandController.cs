﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EnvControlPanel.Models;
using EnvControlPanel.Utils;

namespace EnvControlPanel.Controllers
{
    /// <summary>
    /// Controller for executing a command against a remote server.
    /// </summary>
    /// <remarks>Only the HTTP method POST is supported</remarks>
    public class RemoteCommandController : ApiController
    {
        /// <summary>
        /// HTTP Method POST
        /// </summary>
        /// <remarks>Creates a RemoteCommandRunner object and calls the executeCustomRemoteCommand() method.
        /// The request details are stored inside the rcr request object, and used internally when the executeCustomRemoteCommand()
        /// is invoked.</remarks>
        /// <returns>string</returns>
        public string Post(RemoteCommandRunner remoteCommandRequest)
        {
            
            return remoteCommandRequest.executeCustomRemoteCommand();
        }
    }
}

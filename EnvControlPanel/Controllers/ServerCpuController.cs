﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Diagnostics;
using System.IO;

namespace EnvControlPanel.Controllers
{
    /// <summary>
    /// Controller for getting the remote servers current CPU load.
    /// </summary>
    /// <remarks>Only the HTTP method GET{hostname} is supported</remarks>
    public class ServerCpuController : ApiController
    {


        /// <summary>
        /// HTTP Method GET
        /// </summary>
        /// <remarks>Creates a ServerStats object and calls the getServerCpuLoad(hostname) method.</remarks>
        /// <param name="hostname"></param>
        /// <returns>string</returns>
        public string Get(string hostname)
        {
            string cpuLoad = Models.ServerStats.getServerCpuLoad(hostname);
            int crit = Models.ServerStats.checkCriticalCpu(cpuLoad);
            if (crit == 1)
            {
                return cpuLoad;
            }
            else
            {
                Models.ServerStats.restartServer(hostname);
                return "Heavy load detected.  Server Restarted.";
            }
        }
    }
}

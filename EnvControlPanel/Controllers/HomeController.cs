﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EnvControlPanel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RemoteCommand()
        {
            ViewBag.Message = "Run a Remote Command";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult testPage()
        {
            ViewBag.Message = "Your test page.";

            return View();
        }

        public ActionResult Config()
        {
            ViewBag.Message = "Configurations page.";

            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EnvControlPanel.Controllers
{

    /// <summary>
    /// Controller for getting the remote servers current RAM usage.
    /// </summary>
    /// <remarks>Only the HTTP method GET{hostname} is supported</remarks>
    public class ServerRamController : ApiController
    {


        /// <summary>
        /// HTTP Method GET
        /// </summary>
        /// <remarks>Creates a ServerStats object and calls the getServerRamLoad(hostname) method.</remarks>
        /// <param name="hostname"></param>
        /// <returns>string</returns>
        public string Get(string hostname)
        {
            string ramLoad = Models.ServerStats.getServerRamLoad(hostname);
            int crit = Models.ServerStats.checkCriticalRam(ramLoad);
            if (crit == 1)
            {
                return ramLoad;
            }
            else if (crit == 2)
            {
                return "Conversion Fail.";
            }
            else
            {
                Models.ServerStats.restartServer(hostname);
                return "Heavy load detected.  Server Restarted."; //we won't get here on localhost.
            }
        }
    }
}

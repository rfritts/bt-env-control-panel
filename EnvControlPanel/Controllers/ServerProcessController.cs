﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace EnvControlPanel.Controllers
{
    /// <summary>
    /// Controller for getting the remote servers current total processes running.
    /// </summary>
    /// <remarks>Only the HTTP method GET{hostname} is supported</remarks>
    public class ServerProcessController : ApiController
    {
        /// <summary>
        /// HTTP Method GET
        /// </summary>
        /// <remarks>Creates a ServerStats object and calls the getServerCpuLoad(hostname) method.</remarks>
        /// <param name="hostname"></param>
        /// <returns>string</returns>
        public string Get(string hostname)
        {
            return Models.ServerStats.getTotalProcessesRunning(hostname);
        }
    }
}
